package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {
    @Test
    public void testEmployee(){
        // This is where we create our objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        // Create a sales employee object
        SalesEmployee victoria = new SalesEmployee();
        // Create a new office employee object
        OfficeEmployee michael = new OfficeEmployee();

        // This is where we employ hannah and fire bob
        hannah.employ();
        bob.fire();
        michael.employ();

        // This is where we employ Victoria and give her a Skoda
        victoria.employ();
        victoria.changeCar("Skoda","Y296MHU");

        // This is where we give Michael a deskphone
        michael.changePhone("0117 92 23456");

        // This is where we set Victoria's Salary using Encapsulation
        victoria.setSalary(20000);

        // Let's print their state to the screen
        System.out.println("Hannah employment state = "+ hannah.isEmployed());
        System.out.println("Bob employment state = "+bob.isEmployed());
        System.out.println("Victoria's Car = "+victoria.car +" REG: "+victoria.registration);
        System.out.println("Michael's telephone = "+michael.telePhone);
        System.out.println("Victoria's salary = "+victoria.getSalary());
    }



}
