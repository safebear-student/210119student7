package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data

public class ToolsPageLocators {

        // Messages

        private By successfulLoginMessage = By.xpath(".//body/div[@class='container']/p/b");

        // Search Criteria

        private By searchCriteriaLocator = By.id("toolname");

        // Search Button - note misspelling of search

        private By searchButtonLocator = By.xpath(".//form[@id='seachToolForm'].button");

        // Delete Button

        private By deleteButtonLocator = By.xpath(".//table[@class='table table-striped']/tbody/tr[td='JMeter']//*[@id='remove']");

        // New tool

        private By newtoolButtonLocator = By.xpath(".//button[contains(text(),'New Tool')]");

}
