package com.safebear.auto.pages.locators;
import lombok.Data;
import org.openqa.selenium.By;

@Data

public class LoginPageLocators {

    // fields

    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");

    // buttons

    private By loginButtonLocator = By.id("enter");

    // Check Box

    private By checkboxLocator = By.name("remember");

    // messages

    private By failedLoginMessage = By.xpath(".//p[@id='rejectLogin']/b");

}
