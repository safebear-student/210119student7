package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor

public class ToolsPage {

    ToolsPageLocators locators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle(){
        return driver.getTitle();
    }

    public String checkForLoginSuccessfulMessage(){
        return driver.findElement(locators.getSuccessfulLoginMessage()).getText();
    }

    public void enterSearch (String search) {
        driver.findElement(locators.getSearchCriteriaLocator()).sendKeys(search);
    }

    public void pressEnterToSearch(){
        driver.findElement(locators.getSearchButtonLocator()).click();
    }

    public void deleteTool() {
        driver.findElement(locators.getDeleteButtonLocator()).click();
    }

}
