package com.safebear.auto.nonBDDTests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTests {
    @Test
    public void validLogin(){
        // Step 1.Action: Go to login page
        driver.get(Utils.getURL());
        // Step 1.Expected: Check I'm on login page
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        // Step 2.Action: Login as an valid user
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        // Step 2.Expected: Check I'm on tools page
        Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page");
        // Step 3.Expected: Check success message is shown
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Success"));
    }

    @Test
    public void invalidLogin() {
        // Step 1.Action: Go to login page
        driver.get(Utils.getURL());
        // Step 1.Expected: Check I'm on login page
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle());
        // Step 2.Action: Login as an invalid user
        loginPage.enterUsername("hacker");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        // Step 2.Expected: Check I'm still on login (different method)
        Assert.assertEquals(toolsPage.getPageTitle(), "Login Page");
        // Step 3.Expected: Check failure message is shown
        Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains("incorrect"));
    }

}
