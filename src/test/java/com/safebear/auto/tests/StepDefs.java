package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import com.safebear.auto.utils.Utils;
import org.testng.Assert;

public class StepDefs {

    WebDriver driver;

    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp(){
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown(){
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep","2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
            driver.get(Utils.getURL());
            Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle(), "We're not on the login page or its title has changed");
    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_USER(String userType) throws Throwable {
        switch (userType){
            case "invalidUser":
                loginPage.enterUsername("attacker");
                loginPage.enterPassword("dontletmein");
                loginPage.clickLoginButton();
                break;
            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickLoginButton();
                break;
            default:
                Assert.fail("The test data is wrong - the only values that can be accepted are 'valid user' or 'invalid user'");
                break;
        }
    }

    @Then("^I can see the following message '(.+)'$")
    public void i_can_see_the_following_MESSAGE(String message) throws Throwable {
        switch(message){
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains(message));
                Utils.capturescreenshot(driver,Utils.generateScreenShotFileName("IncorrectPassword"));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(message));
                break;
            default:
                Assert.fail("The test data is wrong");
        }
    }
}
