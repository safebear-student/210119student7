package com.safebear.auto.utils;


import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static org.apache.commons.io.FileUtils.copyFile;


public class Utils {
    private static final String URL = System.getProperty("url","http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER = System.getProperty("browser","chrome");

    public static String getURL() {
        return URL;
    }

    public static String generateScreenShotFileName( String testName){
        // create filename
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+"-"+testName+".png";
    }

    public static void capturescreenshot(WebDriver driver, String fileName){
        // take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        // make sure the screenshot directory exists
        File file = new File("target/screenshots");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created");
            } else {
                System.out.println("Failed to create directory");
            }
        }

        // copy file to filename and location we set before

        try {
            copyFile(scrFile, new File("target/screenshots/"+ fileName));
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    public static WebDriver getDriver(){

        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("windows-size=1366,768");

        System.setProperty("webdriver.gecko.driver","src/test/resources/drivers/geckodriver.exe");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        // firefoxOptions.addArguments("--window-size=1366,768");
        firefoxOptions.addArguments("--width=1366");
        firefoxOptions.addArguments("--height=768");

        System.setProperty("webdriver.ie.driver","src/test/resources/drivers/MicrosoftWebDriver.exe");
        InternetExplorerOptions edgeOptions = new InternetExplorerOptions();
        edgeOptions.addCommandSwitches("windows-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(chromeOptions);

            case "firefox":
                // return new FirefoxDriver();
                return new FirefoxDriver(firefoxOptions);

            case "edge":
                return new InternetExplorerDriver(edgeOptions);

            case "headless":
                chromeOptions.addArguments("headless", "disable-gpu");
                return new ChromeDriver(chromeOptions);

            default:
                return new ChromeDriver(chromeOptions);
        }
    }
}
